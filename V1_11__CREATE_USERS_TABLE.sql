create table IF NOT EXISTS users (
  id_user INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(50) NOT NULL,
  lastname VARCHAR(30) NOT NULL,
  address VARCHAR(30) NOT NULL
);
